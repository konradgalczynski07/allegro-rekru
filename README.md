
# Hi Allegro Team! 👋

Thank you for your little assigment. Here is my answer. You'll find it in the Pull Request.


# Instalation:

To run app using docker:
```
docker-compose up
```

or create virtualenv with your favourite tool, install dependencies and run app:

```
python3 -m venv venv
pip install -r requirements.txt
python allegro-rekru/app.py
```


# Unit tests

To run tests use:

```
cd allegro-rekru && python -m unittest
```


# Docs available under

`0.0.0.0:8000/docs`